const amqp = require("amqplib")

const connectionRetryTimeoutMs = 30000
let channel = null

const connect = async (queues, exchanges, options) => {
  const connectionString = options.connectionString || process.env.RABBIT_URL

  if (!connectionString) {
    throw new Error("No connectionstring specified for rabbitmq. Either set env variable RABBIT_URL or send in a connectionstring")
  }

  try {
    const connection = await amqp.connect(connectionString)
    channel = await connection.createChannel()

    if (options.prefetch) {
      await channel.prefetch(options.prefetch)
    }

    await assertExchanges(exchanges)
    await assertQueues(queues)

    console.log("Rabbit connected, jump around!")
  } catch (err) {
    console.error(`rabbit connection error, retrying in ${connectionRetryTimeoutMs/1000}s... `, JSON.stringify(err))
    setTimeout(() => connect(queues, exchanges, connectionString), connectionRetryTimeoutMs)
  }
}

const disconnect = async () => {
  await channel.close()
}

const publish = function publish(exchange, data) {
  try {
    channel.publish(exchange, "", new Buffer.from(JSON.stringify(data)))
  } catch(err) {
    console.error(`Error sending message on exchange ${exchange}`, data, JSON.stringify(err))
  }
}

const errorStringification = () => {
  var config = {
    configurable: true,
    value: function() {
      var alt = {};
      var storeKey = function(key) {
        alt[key] = this[key];
      };
      Object.getOwnPropertyNames(this).forEach(storeKey, this);
      return alt;
    }
  };
  Object.defineProperty(Error.prototype, 'toJSON', config);
}

const setupDlx = async (queue) => {
  const dlx = {
    name:`${queue.name}_dlx`,
    type: "fanout",
    options: null
  }

  const dlxQueue = {
    name: `${queue.name}_retry`,
    binding: dlx.name,
    options: {
      messageTtl: queue.dlx.messageTtl || 300000,
      deadLetterExchange: queue.binding
    }
  }
  queue.options.deadLetterExchange = dlx.name

  if (typeof(queue.dlx.deadQueue) === 'undefined' || queue.dlx.deadQueue === null) {
    queue.dlx.deadQueue = true
  }

  if (queue.dlx.deadQueue) {
    await channel.assertQueue(`${queue.name}_dead`)
  }

  await assertExchanges([dlx])
  await channel.assertQueue(dlxQueue.name, dlxQueue.options)
  await channel.bindQueue(dlxQueue.name, dlxQueue.binding)
}

const assertQueues = async (queues) => {
  errorStringification()

  for (const queue of queues) {
    try {
      queue.options = queue.options || {
        durable: true
      }

      if (queue.dlx) {
        await setupDlx(queue)
      }

      await channel.assertQueue(queue.name, queue.options)
      await channel.bindQueue(queue.name, queue.binding)

      setTimeout(() => { //run this detached
        channel.consume(queue.name, async function(msg) {
          try {
            await queue.callback(msg, channel)

            channel.ack(msg)
          } catch(err) {
            console.error("Error processing message", msg.content.toString(), JSON.stringify(err))
            const death = (msg.properties.headers["x-death"] || [])
              .find(x => x.exchange === queue.binding)

            if (queue.dlx && queue.dlx.deadQueue && death && death.count > 5) {
              console.error("Fatal processing message, putting on dead queue", msg.content.toString())
              channel.sendToQueue(
                `${queue.name}_dead`,
                new Buffer.from(JSON.stringify({
                  data: msg.content.toString(),
                  error: err
                }))
              )
              channel.ack(msg)
            } else {
              queue.dlx
                ? channel.nack(msg, false, false)
                : channel.ack(msg) // always ack message to not get stuck in endless loop blocking all other messages
            }
          }
        }, {noack: false})
      }, 0)
    } catch (err) {
      console.error(`Error asserting queue ${queue.name}`, err)
    }
  }
}

const assertExchanges = async (exchanges) => {
  for (const exchange of exchanges) {
    try {
      await channel.assertExchange(exchange.name, exchange.type, exchange.options)
    } catch (err) {
      console.error(`Error asserting exchange ${exchange}`, err)
    }
  }
}

module.exports = {
  connect: connect,
  publish: publish,
  disconnect: disconnect,
}
